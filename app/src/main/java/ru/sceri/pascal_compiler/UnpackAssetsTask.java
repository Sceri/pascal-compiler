package ru.sceri.pascal_compiler;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import sharedcode.turboeditor.activity.MainActivity;

/**
 * Created by Sceri on 29.10.2015.
 */
public class UnpackAssetsTask extends AsyncTask<Integer, Integer, Void> {

    private MainActivity activity;
    private ProgressDialog progressDialog;

    private boolean unpack = false;

    private String[] archives = new String[] { "binaries.zip", "units.zip" };
    private int[] archivesSize = new int[] { 5, 187 };
    private String[] binaries = new String[] { "as", "busybox", "fpc", "ld", "ptop" };

    public UnpackAssetsTask(MainActivity activity) {
        this.activity = activity;

        progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected Void doInBackground(Integer... params) {
        if(unpack) {
            try {
                org.apache.commons.io.FileUtils.forceMkdir(new File(activity.getUnitsPath()));

                unzip(0, new File(activity.getFilesDirectory()));
                unzip(1, new File(activity.getUnitsPath()));
            } catch (IOException ignored) {
                ignored.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... params) {
        progressDialog.setMessage("Распаковка " + archives[params[0]] + ".\nПожалуйста подождите...\n" + params[1] + " из " + archivesSize[params[0]] + " распаковано");
    }

    private void unzip(int archive, File path) throws IOException {
        ZipInputStream zis = new ZipInputStream(activity.getAssets().open(archives[archive]));

        int counter = 0;

        ZipEntry zipEntry;
        while ((zipEntry = zis.getNextEntry()) != null) {
            counter++;

            File entryFile = new File(path, zipEntry.getName());
            if (!entryFile.getAbsolutePath().contains("META-INF")) {
                if (zipEntry.isDirectory()) {
                    org.apache.commons.io.FileUtils.forceMkdir(entryFile);
                } else {
                    FileOutputStream output = org.apache.commons.io.FileUtils.openOutputStream(entryFile);
                    try {
                        IOUtils.copy(zis, output);
                        output.close();
                    } finally {
                        IOUtils.closeQuietly(output);
                    }
                }
            }

            activity.setExecutable(entryFile);

            publishProgress(archive, counter);
        }

        zis.close();
    }

    public boolean checkBinaries() {
        for(String binary : binaries) {
            if(!new File(activity.getFilesDirectory(), binary).exists()) {
                return false;
            }
        }

        return true;
    }

    @Override
    protected void onPreExecute() {
        if(!new File(activity.getUnitsPath()).exists() || !checkBinaries()) {
            unpack = true;

            progressDialog.setMessage("Распаковка файлов.\nПожалуйста подождите...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
